# ansible-role-node-exporter

This role installs and maintains prometheus node-exporter.

## Description

It is also possible to copy scripts from local machine to the target and have a systemd timer which schedules the script.
That makes it possible, to maintain additional metrics inside the textfile directory. See [textfile](https://github.com/prometheus/node_exporter#textfile-collector).

## Role Variables

| Name                                |                                           Default                                            | Description                                                                                                                                                                                                            |
| :---------------------------------- | :------------------------------------------------------------------------------------------: | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `node_exporter_version`             |                                            1.0.1                                             | Default Node Exporter version to install. Change here to up or downgrade.                                                                                                                                              |
| `node_exporter_user`                |                                        node_exporter                                         | Username for Node Exporter under which it is running.                                                                                                                                                                  |
| `node_exporter_group`               |                                        node_exporter                                         | Groupname for Node Exporter under which it is running.                                                                                                                                                                 |
| `node_exporter_additional_groups`   |                                              []                                              | List of groups to append node_exporter user to. Groups must be present.                                                                                                                                                |
| `node_exporter_dist_url`            | "https://github.com/prometheus/node_exporter/releases/download/v{{ node_exporter_version }}" | URL to get node exporter releases from.                                                                                                                                                                                |
| `node_exporter_install_dir`         |                                      /opt/node_exporter                                      | Node Exporter home directory.                                                                                                                                                                                          |
| `node_exporter_config_dir`          |                                      /etc/node_exporter                                      | Node Exporter configuration directory.                                                                                                                                                                                 |
| `node_exporter_textfile_dir`        |                                    /var/lib/node_exporter                                    | Node Exporter textfile directory.                                                                                                                                                                                      |
| `node_exporter_scripts`             |                                              []                                              | List of installable scripts, the will be copied to the target and a systemd timer/service unit will be installed to run. For complete syntax see [defaults/main.yml](defaults/main.yml)                                |
| `node_exporter_enable_tls`          |                                             true                                             | Create self-signed TLS certificate to secure communication.                                                                                                                                                            |
| `node_exporter_domain_name`         |                                  node_exporter.example.com                                   | DNS domain name used in self-signed certificate.                                                                                                                                                                       |
| `node_exporter_tls_server_config`   |                                              {}                                              | Configuration map for TLS part in web-config.yml file. Syntax is the same as in original config. See [Sample web-config.yml](https://github.com/prometheus/node_exporter/tree/master/https#sample-config)              |
| `node_exporter_http_server_config`  |                                              {}                                              | Configuration map for HTTP part in web-config.yml file. Syntax is the same as in original config. See [Sample web-config.yml](https://github.com/prometheus/node_exporter/tree/master/https#sample-config)             |
| `node_exporter_basic_auth_users`    |                                              {}                                              | Basic authentication user. Key: username, Value: password. Password will be automatically hashed with bcrypt. See [Sample web-config.yml](https://github.com/prometheus/node_exporter/tree/master/https#sample-config) |
| `node_exporter_enabled_collectors`  |                                          [systemd]                                           | Collectors enabled additionally to the default collectors. See [Collectors disabled by default](https://github.com/prometheus/node_exporter#disabled-by-default)                                                       |
| `node_exporter_disabled_collectors` |                                              []                                              | Collectors to disable.                                                                                                                                                                                                 |
| `node_exporter_web_listen_address`  |                                        "0.0.0.0:9100"                                        | Address on which to expose metrics and web interface.                                                                                                                                                                  |
| `node_exporter_telemetry_path`      |                                           /metrics                                           | Path under which to expose metrics.                                                                                                                                                                                    |
| `node_exporter_max_request`         |                                              0                                               | Maximum number of parallel scrape requests. Use 0 to disable.                                                                                                                                                          |
| `node_exporter_log_level`           |                                             info                                             | Only log messages with the given severity or above. One of: [debug, info, warn, error]                                                                                                                                 |
| `node_exporter_log_fmt`             |                                            logfmt                                            | Output format of log messages. One of: [logfmt, json]                                                                                                                                                                  |

## Role Tags

| Name                    | Description                    |
| ----------------------- | ------------------------------ |
| node_exporter_all       | Enable all tasks of this role. |
| node_exporter_install   | Enable only install tasks.     |
| node_exporter_configure | Enable only configure tasks.   |
| node_exporter_scripts   | Enable only scripts tasks.     |

## Dependencies

**None**

## Example Playbook

See [molecule.yml](molecule/default/converge.yml)

## License

MIT License

## Contributors

Daniel von Essen
