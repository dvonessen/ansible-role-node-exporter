#!/bin/bash

export ANSIBLE_VAULT_PASSWORD_FILE=$(pwd)/tests/vault/password

PARAMETERS=$@

if [[ ${#PARAMETERS[@]} -gt 1 ]]
then
  molecule $@
else
  molecule test
fi
